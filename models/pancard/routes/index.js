const controller = require("../controller/index");
const express = require("express");
const router = express.Router();
router.post("/verification", controller.verification);
module.exports = router;
