
const HttpStatus = require("http-status-codes");

const puppeteer = require('puppeteer');

require('dotenv').config()

const {logger} = require("../../../utils/logger");

const keys = require("../../../keys.json");


module.exports.verification = async (req, res) => {
  const { pancardNumber , dob , fullName} =  req.body;
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.goto(keys.panUrl);
  await page.type('[name="pan"]', pancardNumber);
  await page.type('[name="dateOfBirth"]', dob);
  await page.type('[name="fullName"]', fullName);

  const captchaElement = await page.$('#captchaImg');
  const imageBuffer = await captchaElement.screenshot();

  const code = await convertImageToText(imageBuffer);
  await page.waitFor(1000);
  await page.type('[name="captchaCode"]', code);
  const cal = await page.$$('#submitbtn');
    if(cal && cal.length) return await page.click('#submitbtn')[0];
    await page.waitFor(1000);
    response = await (await (await page.$('.success')).getProperty('textContent')).jsonValue();
    if (response) {
      res.status(HttpStatus.OK).json({ success: true, data: response });
      await browser.close();
    } else {
      res.status(HttpStatus.OK).json({ success: false, data: 'Invalid pancard' });
      await browser.close();
    }
};


async function convertImageToText (buffer) {
  const vision = require("@google-cloud/vision");
  const client = new vision.ImageAnnotatorClient();
  // requesting OCR
  const [result] = await client.textDetection(buffer);
  var detections = result.textAnnotations;
  if (detections && detections.length) {
    return(detections[0].description);
  } else {
    logger.info("captcha image not found ocr")
    return false;
  }
}

