
const HttpStatus = require("http-status-codes");


const puppeteer = require('puppeteer');

require('dotenv').config()

const {logger} = require("../../../utils/logger");


const keys = require("../../../keys.json");



async function retry(page) {
  return new Promise(async (resolve, reject) => {
    let captchaElement = await page.$('table td img');
    const imageBuffer = await captchaElement.screenshot();
    const code = await OcrImagetoText(imageBuffer);
    await page.type('[name="form_rcdl:j_idt42:CaptchaID"]', code);
    await page.click('button span.ui-button-text')[0];
    await page.waitFor(2000);
    let results = await page.$$('table.table.table-responsive.table-striped.table-condensed.table-bordered tr td');
    if (results && results.length) {
      resolve(true);
    } else {
      resolve(false);
    }
  });
}

module.exports.verification = async (req, res) => {

  // oper brower
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  let data = [];

  await page.goto(keys.dlUrl);

  await page.waitFor(3000);

  await page.type('[name="form_rcdl:tf_dlNO"]', keys.dlNumber);

  await page.waitFor(2000);

  await page.type('[name="form_rcdl:tf_dob_input"]', keys.dob);

  const cal = await page.$$('#ui-datepicker-div');
  if (cal) {
    await page.waitFor(1000);
    await page.click('#ui-datepicker-div')[0];
  }
  await page.screenshot({ path: '1.png' });

  await page.waitFor(4000);

  let captchaElement = await page.$('table td img');
  const imageBuffer = await captchaElement.screenshot();

  
  const code = await OcrImagetoText(imageBuffer);

  await page.waitFor(2000);

  await page.type('[name="form_rcdl:j_idt42:CaptchaID"]', code);

  await page.waitFor(1000);
  let retrytime = 3;
  const submitbtn = await page.$$('button span.ui-button-text');
  if (submitbtn) {
    await page.click('button span.ui-button-text')[0];
    await page.waitFor(2000);
    let results = await page.$$('table.table.table-responsive.table-striped.table-condensed.table-bordered tr td');
    if (results && results.length === 0) {
      for (let index = 0; index < retrytime; index++) {
        if (await retry(page)) {
          break;
        }
      }
    }
    let key = ''
    let PromiseArray = [];
    results.forEach((e, i) => {
      PromiseArray.push(new Promise(async (resolve) => {
        if ((i + 1) % 2 !== 0) {
          key = await (await e.getProperty('textContent')).jsonValue();
        }
        let value = await (await e.getProperty('textContent')).jsonValue();
        if ((i + 1) % 2 === 0) {
          data.push({
            [`${key}_key`]: value
          });
        }
        resolve(true);
      })
      );
    });
    Promise.all(PromiseArray).then(async function (values) {
      res.status(HttpStatus.OK).json({ success: true, data, message: 'Entry found' });
      await browser.close();
    });
  } else {
    res.status(HttpStatus.OK).json({ success: false, data, message: 'Captcha Failed' });
    await browser.close();
  }

};

async function OcrImagetoText(buffer) {
  return new Promise(async (resolve, reject) => {
    try {
      const vision = require("@google-cloud/vision");
      const client = new vision.ImageAnnotatorClient();
      // requesting OCR
      const [result] = await client.textDetection(buffer);
      var detections = result.textAnnotations;
      if (detections && detections.length) {
        resolve(detections[0].description);
      } else {
        logger.info("captcha image not found ocr")
        reject('captcha image not found ocr');
      }
    } catch (error) {
      logger.info("captcha image not found ocr" , error)
      reject('captcha image not found ocr', error);
    }
  });
}


