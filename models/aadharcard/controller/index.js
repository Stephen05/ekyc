
const HttpStatus = require("http-status-codes");
const fs = require("fs");
const path = require('path');
const unzipper = require('unzipper');
let xmlParser = require('xml2json');
const puppeteer = require('puppeteer');
require('dotenv').config()

const { logger } = require("../../../utils/logger");
const keys = require("../../../keys.json");


let manageUsersData = new Map();

function zipFileToJSON(pathname, callback) {
  fs.readdir(pathname, async function (err, files) {
    if (err && !(files && files.length)) {
      logger.info("Directory does not exist.");
      callback(false);
      return;
    }
    const directory = await unzipper.Open.file(`${pathname}/${files[0]}`);
    const extracted = await directory.files[0].buffer('1234');
    const xmlString = extracted.toString();
    fs.unlink(`${pathname}/${files[0]}`, err => {
      if (err) throw err;
    });
    callback(xmlParser.toJson(xmlString));
  });
}

async function convertImageToText(buffer) {
  const vision = require("@google-cloud/vision");
  const client = new vision.ImageAnnotatorClient();
  // requesting OCR
  const [result] = await client.textDetection(buffer);
  var detections = result.textAnnotations;
  if (detections && detections.length) {
    return (detections[0].description);
  }
  logger.info("captcha image not found ocr")
  return false;
}

module.exports.verifyOtp = async (req, res) => {

  const { aadharNumber } = req.body;
  const pathname = path.join(__dirname, `../../../${keys.aadharFiles}/${aadharNumber}`);
  const browser = await puppeteer.launch({ headless: true });
  const page = await browser.newPage();
  await page._client.send('Page.setDownloadBehavior', {
    behavior: 'allow',
    downloadPath: pathname
  });
  await page.goto(keys.aadharUrl);
  await page.type('[name="uidno"]', aadharNumber);

  const captchaElement = await page.$('#captcha-img');
  const imageBuffer = await captchaElement.screenshot();

  //  captcha image send to OCR
  let captcha_code = await convertImageToText(imageBuffer);
  captcha_code = captcha_code ? captcha_code.trim() : '';
  logger.info(captcha_code, 'captcha_code');

  // otp field
  await page.type('[name="security_code"]', captcha_code);

  const submitButton = await page.$$('#smt_btn');
  if (submitButton) {
    await page.click('#smt_btn')[0];
    await page.waitFor(1000);
    await page.click('#smt_btn')[0];
    await page.waitFor(2000);

    // captcha error
    let captchaError = await page.$$('.alert-error');
    if (captchaError && captchaError.length) {
      captchaError = await (await (await page.$('.alert-error')).getProperty('textContent')).jsonValue();
      return res.status(HttpStatus.OK).json({ success: false, data: captchaError });
    }
    manageUsersData.set(aadharNumber, page);
    return res.status(HttpStatus.OK).json({ success: true, message: 'otp sent' })
  }
};

module.exports.getaadharDetails = async (req, res) => {
  const { aadharNumber, otp } = req.body;
  const pathname = path.join(__dirname, `../../../${keys.aadharFiles}/${aadharNumber}`);
  if (manageUsersData.has(aadharNumber) && otp) {
    let page = manageUsersData.get(aadharNumber);
    manageUsersData.delete(aadharNumber);
    await page.type('[name="zipcode"]', '1234');
    await page.type('[name="totp"]', otp);
    await page.click('button.smt-totp')[0];
    await page.waitFor(2000);
    zipFileToJSON(pathname, async function (data) {
      if (data) return res.status(HttpStatus.OK).json({ success: true, data });
      let otpError = await page.$$('.alert-error');
      if (otpError && otpError.length) {
        otpError = await (await (await page.$('.alert-error')).getProperty('textContent')).jsonValue();
        res.status(HttpStatus.OK).json({ success: false, data: otpError });
      }
    });
  } else {
    res.status(HttpStatus.OK).json({ success: false, data: 'otp page expired!' });
  }
};

