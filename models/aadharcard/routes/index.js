const controller = require("../controller/index");
const express = require("express");
const router = express.Router();

router.post("/verifyOtp", controller.verifyOtp);

router.post("/getaadharDetails", controller.getaadharDetails);

module.exports = router;




