const express = require("express");

const bodyParser = require("body-parser");

const { logger } = require("./utils/logger");


const app = express();

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({ extended: false }));

const aadharRoute = require("./models/aadharcard/routes");
const pancardRoute = require("./models/pancard/routes");
const dlRoute = require("./models/drivingLicense/routes");

app.use("/aadhar", aadharRoute);

app.use("/pancard", pancardRoute);

app.use("/drivinglicense", dlRoute);

logger.info("All Routes are Running Sucesssfully");

module.exports = app;
