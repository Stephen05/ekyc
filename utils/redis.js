const redis = require("redis");

const { logger } = require("../utils/logger");

try {
  logger.info('redis Cache');
  client = new redis.RedisClient({
    host: process.env.redishost,
    port: 6379,
    password: 123456,
    retry_strategy: (options) => {
      if (options.error && options.error.code === 'ECONNREFUSED') {
        // End reconnecting on a specific error and flush all commands with
        // a individual error
        return new Error('The server refused the connection');
      }
      if (options.total_retry_time > 1000 * 60 * 60) {
        // End reconnecting after a specific timeout and flush all commands
        // with a individual error
        return new Error('Retry time exhausted');
      }
      if (options.attempt > 10) {
        // End reconnecting with built in error
        return new Error('Retry attempts exceeded');
      }
      // reconnect after
      return Math.min(options.attempt * 100, 3000);
    }
  });
} catch (error) {
  logger.error(error, 'error');
}

module.exports = {
  getCached: async (redis_key) => {
    return new Promise((resolve) => {
      client.get(redis_key, function (err, data) {
        if (err) {
          logger.info(err);
          resolve(false);
        } else {
          resolve(JSON.parse(data));
        }
      });
    });
  },
  caching: (key, data) => {
    client.set(key, JSON.stringify(data))
  },
  delCache: (key) => {
    client.del(key)
  }
}